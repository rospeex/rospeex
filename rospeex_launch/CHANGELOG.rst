^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
Changelog for package rospeex_launch
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

3.0.1 (2017-04-19)
------------------
* Mod microphone gain and VAD parameters

3.0.0 (2017-04-05)
------------------

2.15.4 (2017-02-09)
-------------------

2.15.3 (2017-01-31)
-------------------
* Mod update microsoft speech api from Oxford Project to Cognitive Services

2.15.2 (2017-01-13)
-------------------

2.15.1 (2017-01-12)
-------------------
* Add vad off config during playing sound

2.15.0 (2016-12-07)
-------------------

2.14.7 (2016-03-30)
-------------------

2.14.6 (2016-01-14)
-------------------

2.14.5 (2016-01-14)
-------------------

2.14.4 (2015-12-01)
-------------------

2.14.3 (2015-12-01)
-------------------

2.14.2 (2015-11-26)
-------------------
* Add docomo API and Microsoft API to rospeex_core

2.14.1 (2015-09-18)
-------------------

2.14.0 (2015-09-18)
-------------------

2.13.0 (2015-09-14)
-------------------
* Fixed launch files

2.12.6 (2015-04-24)
-------------------

2.12.5 (2015-03-03)
-------------------

2.12.4 (2015-02-19)
-------------------

2.12.3 (2015-02-13)
-------------------

2.12.2 (2015-01-07)
-------------------

2.12.1 (2014-12-26)
-------------------
* fix setupscript and package.xml
* fix ss / sr config files, and fix rospeex_launch package.
* modify package.xml
* fix comment and modify file layout.
* modify lanch files, and rospeex_web audiomonitor
* add rospeex_launch package
