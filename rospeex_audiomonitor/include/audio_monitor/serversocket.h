#ifndef SERVERSOCKET_H
#define SERVERSOCKET_H

#include <QObject>
#include <QTcpSocket>
#include <QHostAddress>

class ServerSocket : public QObject
{
    Q_OBJECT
public:
    explicit ServerSocket(int socketDescriptor, QObject *parent = 0);

    void SendStart();
    void SendData( const void *data, int len );
    void SendEnd();
    void SendCancel();
    void SendKeyPressed( int val );
    void SendEpdOn();
    void SendEpdOff();

    QHostAddress peerAddress() const;

private slots:
    void clientDisconnected();
    void readyRead();

signals:
    void disconnected();
    void read(const QString &cmd);
    void error(QTcpSocket::SocketError socketError);

private:
    QTcpSocket socket;
    bool m_bSendingData;

    void _Send( const QString& str );
    void _Send( const void *data, int len );

};

#endif // SERVERSOCKET_H
