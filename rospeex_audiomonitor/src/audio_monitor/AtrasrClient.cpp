#include "AtrasrClient.h"
//#include "wavefile.h"
#include <QtEndian>
#include <QDebug>

static void _AtrasrSwapFrameData( unsigned char *pBuf, size_t len )
{
    unsigned char tmp;
    for( ; len > 0; len -= 2 )
    {
        tmp = *pBuf;
        *pBuf = *(pBuf+1);
        *(pBuf+1) = tmp;

        pBuf += 2;
    }
}

static void _AtrasrInitSendBuf( ATRASR_CLIENT *client )
{
    client->curIndexSendBuf = ATRASR_FRAME_HEADER_SIZE;
}

static void _AtrasrInitRecvBuf( ATRASR_CLIENT *client )
{
    client->curIndexRecvBuf = 0;
}

int AtrasrConnect( ATRASR_CLIENT *client, const char *srvHost, int srvPort )
{
    _AtrasrInitSendBuf( client );
    _AtrasrInitRecvBuf( client );
    client->sendFrameCount = 0;
    client->recvFrameCount = 0;
    client->logFilename = NULL;
	
    client->socket = new QTcpSocket();
    client->socket->connectToHost(srvHost, srvPort);
    if (client->socket->waitForConnected(5000)){
        return 0;
    }
    else{
        AtrasrClose(client);
    }

    qCritical() << QString("Could not connect to %1(%2)").arg(srvHost).arg(srvPort);
    return -1;
}

void AtrasrClose( ATRASR_CLIENT *client )
{
    if ( client->socket != NULL )
    {
        client->socket->close();
        delete client->socket;
        client->socket = NULL;
    }
}

static int _AtrasrSendFrame( ATRASR_CLIENT *client, ATRASR_FRAME_TYPE type )
{
    int nRet = -1;
    int nSent;

    if ( client->socket != NULL )
    {
        memset( client->sendBuf, 0, ATRASR_FRAME_SIZE );
        *((long *)&client->sendBuf) = type;
        
        nSent = client->socket->write((const char*)client->sendBuf, ATRASR_FRAME_SIZE);
        if( nSent < ATRASR_FRAME_SIZE )
        {
            qCritical() << "Could not send to ATRASR : frame type " << type;
        }
        else
        {
            nRet = 0;
            client->sendFrameCount++;
        }
    }

    return nRet;
}

int AtrasrSendTOF( ATRASR_CLIENT *client )
{
    _AtrasrInitSendBuf( client );

    client->sendFrameCount = 0;
    client->recvFrameCount = 0;

    //fprintf( stderr, "ATRASR : send TOF\n" );
    return _AtrasrSendFrame( client, ATRASR_TOF );
}

int AtrasrSendEOF( ATRASR_CLIENT *client )
{
    //fprintf( stderr, "ATRASR : send EOF\n" );
    return _AtrasrSendFrame( client, ATRASR_EOF );
}

static int _AtrasrSendDATA( ATRASR_CLIENT *client, const char *data, size_t size )
{
    int nRet = -1;
    size_t sizeRemain;
    size_t sendLen;
    int nSent;
    unsigned char *pSendBuf = client->sendBuf;

    if ( client->socket != NULL )
    {
        for (;;)
        {
            sizeRemain = ATRASR_FRAME_SIZE - ( client->curIndexSendBuf % ATRASR_FRAME_SIZE );
            if ( size < sizeRemain )
            {
                memcpy( client->sendBuf + client->curIndexSendBuf, data, size );
                client->curIndexSendBuf += size;
                break;
            }
            else
            {
                *((long *)pSendBuf) = ATRASR_DATA;
                memcpy( client->sendBuf + client->curIndexSendBuf, data, sizeRemain );

                client->curIndexSendBuf += sizeRemain + ATRASR_FRAME_HEADER_SIZE;
                data += sizeRemain;
                size -= sizeRemain;
                pSendBuf += ATRASR_FRAME_SIZE;
            }
        }

        sendLen = pSendBuf - client->sendBuf;

        if ( sendLen > 0 )
        {
            nSent = client->socket->write((const char*)client->sendBuf, sendLen);
            if ( nSent < sendLen )
            {
                qCritical() << "Could not send to ATRASR : frame[" << client->sendFrameCount << "]";
            }
            else
            {
                nRet = 0;
                client->sendFrameCount += sendLen / ATRASR_FRAME_SIZE;
            }
        }
        else
        {
            nRet = 0;
        }

        // move memory to head.
        client->curIndexSendBuf = client->curIndexSendBuf % ATRASR_FRAME_SIZE;
        memcpy( client->sendBuf + ATRASR_FRAME_HEADER_SIZE, pSendBuf + ATRASR_FRAME_HEADER_SIZE,
            client->curIndexSendBuf - ATRASR_FRAME_HEADER_SIZE );
    }

    return nRet;
}

int AtrasrSendDATA( ATRASR_CLIENT *client, const char *data, size_t size )
{
    int nRet = -1;
    int sizeMaxSend = ATRASR_FRAME_DATA_SIZE * ATRASR_SEND_BUF_FRAME_COUNT;

    for (;;) 
    {
        if ( size <= sizeMaxSend )
        {
            nRet = _AtrasrSendDATA( client, data, size );
            break;
        }
        else
        {
            nRet = _AtrasrSendDATA( client, data, sizeMaxSend );
            if( nRet != 0 )
            {
                break;
            }
            data += sizeMaxSend;
            size -= sizeMaxSend;
        }
    }

    return nRet;
}

int AtrasrSendSTARTPU( ATRASR_CLIENT *client )
{
    _AtrasrInitSendBuf( client );

    //fprintf( stderr, "ATRASR : send STARTPU\n" );
    return _AtrasrSendFrame( client, ATRASR_START );
}

int AtrasrSendENDPU( ATRASR_CLIENT *client )
{
    //fprintf( stderr, "ATRASR : send ENDPU\n" );
    return _AtrasrSendFrame( client, ATRASR_END );
}

int AtrasrSendCANCELPU( ATRASR_CLIENT *client )
{
    //fprintf( stderr, "ATRASR : send CANCEL\n" );
    return _AtrasrSendFrame( client, ATRASR_CANCEL );
}

int AtrasrReceiveFrame( ATRASR_CLIENT *client )
{
    int frameCount = -1;
    int sizeRecv;
    size_t sizeRemain;
    int oldIndex;

    if ( client->socket != NULL )
    {
        // move memory to head.
        oldIndex = client->curIndexRecvBuf;
        client->curIndexRecvBuf = oldIndex % ATRASR_FRAME_SIZE;
        memcpy( client->recvBuf, client->recvBuf + oldIndex - client->curIndexRecvBuf, client->curIndexRecvBuf );

        sizeRemain = ATRASR_RECV_BUF_SIZE - client->curIndexRecvBuf;
        sizeRecv = client->socket->read( (char*)client->recvBuf + client->curIndexRecvBuf, sizeRemain );
        if ( sizeRecv > 0 )
        {
            client->curIndexRecvBuf += sizeRecv;
            frameCount = client->curIndexRecvBuf / ATRASR_FRAME_SIZE;
            
            client->recvFrameCount += frameCount;
        }
        else
        {
            //fprintf( stderr, "Receive frame error from ATRASR.\n" ); 
            frameCount = -1;
            AtrasrClose(client);
        }
    }

    return frameCount;
}

unsigned char *AtrasrParseFrame( ATRASR_CLIENT *client, int nIndex, long *type )
{
    unsigned char *pRecvBuf = client->recvBuf + ATRASR_FRAME_SIZE * nIndex;

    // Big endian to little endian.
    *type = qFromBigEndian<qint32>( pRecvBuf );
    //*type = *((long *)pRecvBuf);
    _AtrasrSwapFrameData( pRecvBuf + ATRASR_FRAME_HEADER_SIZE, ATRASR_FRAME_DATA_SIZE );

    return pRecvBuf + ATRASR_FRAME_HEADER_SIZE;
}
