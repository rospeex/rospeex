/****************************************************************************
**
** Copyright (C) 2009 Nokia Corporation and/or its subsidiary(-ies).
** All rights reserved.
** Contact: Nokia Corporation (qt-info@nokia.com)
**
** This file is part of the examples of the Qt Toolkit.
**
** $QT_BEGIN_LICENSE:LGPL$
** Commercial Usage
** Licensees holding valid Qt Commercial licenses may use this file in
** accordance with the Qt Commercial License Agreement provided with the
** Software or, alternatively, in accordance with the terms contained in
** a written agreement between you and Nokia.
**
** GNU Lesser General Public License Usage
** Alternatively, this file may be used under the terms of the GNU Lesser
** General Public License version 2.1 as published by the Free Software
** Foundation and appearing in the file LICENSE.LGPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU Lesser General Public License version 2.1 requirements
** will be met: http://www.gnu.org/licenses/old-licenses/lgpl-2.1.html.
**
** In addition, as a special exception, Nokia gives you certain additional
** rights.  These rights are described in the Nokia Qt LGPL Exception
** version 1.1, included in the file LGPL_EXCEPTION.txt in this package.
**
** GNU General Public License Usage
** Alternatively, this file may be used under the terms of the GNU
** General Public License version 3.0 as published by the Free Software
** Foundation and appearing in the file LICENSE.GPL included in the
** packaging of this file.  Please review the following information to
** ensure the GNU General Public License version 3.0 requirements will be
** met: http://www.gnu.org/copyleft/gpl.html.
**
** If you have questions regarding the use of this file, please contact
** Nokia at qt-info@nokia.com.
** $QT_END_LICENSE$
**
****************************************************************************/

#include <QtGui>
#include "audioinput.h"
#include "soundserver.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    app.setApplicationName("Audio Monitor");

    int port = 0;
    QString epdHost;
    QStringList args = app.arguments();
    for (int i = 1; i < args.count(); i++){
        if (args[i] == "-filename"){
            i++;
            if (i < args.count()){
                AudioInfo::setFileName(args[i]);
            }
        }
        else if (args[i] == "-port"){
            i++;
            if (i < args.count()){
                port = args[i].toInt();
            }
        }
        else if (args[i] == "-epd"){
            i++;
            if (i < args.count()){
                QStringList epdArgs = args[i].split(":");
                if (epdArgs.count() == 2){
                    epdHost = epdArgs[0];
                    _epdPort = epdArgs[1].toInt();
                }
                else{
                    _epdPort = args[i].toInt();
                }
            }
        }
    }

    SoundServer server;
    if (port > 0){
        if (!server.listen(QHostAddress::Any, port)) {
            QMessageBox::critical(NULL, "Sound Server",
                QString("Unable to start the server: %1.").arg(server.errorString()));
            return -1;
        }
    }
    _atrasrClient.socket = NULL;
    if (!epdHost.isEmpty() && _epdPort > 0){
        if ( AtrasrConnect( &_atrasrClient, epdHost.toLatin1(), _epdPort ) != -1 ){
            AtrasrSendTOF( &_atrasrClient );
            AtrasrSendSTARTPU( &_atrasrClient );
        }      
    }

    SoundServerApp input(&server);
    input.resize(240,240);
    input.show();

    int ret = app.exec();
    if (port > 0){
        server.close();
    }
    if (_epdPort > 0){
        AtrasrSendEOF( &_atrasrClient );
        AtrasrClose( &_atrasrClient );
    }
    return ret;
}
