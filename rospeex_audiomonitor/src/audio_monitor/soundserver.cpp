#include "soundserver.h"
#include <QDebug>

SoundServer::SoundServer(QObject *parent) :
    QTcpServer(parent)
{
}

void SoundServer::incomingConnection(int socketDescriptor)
{
    ServerSocket *sock = new ServerSocket(socketDescriptor, this);
    connect(sock, SIGNAL(disconnected()), this, SLOT(socketFinished()));
    connect(sock, SIGNAL(read(QString)), this, SLOT(read(QString)));

    sockets.append(sock);
    emit clientConnected(sock->peerAddress());
}

void SoundServer::socketFinished()
{
    ServerSocket *sock = (ServerSocket*)sender();
    sockets.removeOne(sock);
    sock->deleteLater();
}

void SoundServer::read(const QString &cmd)
{
    emit onCmd(cmd);
}

void SoundServer::SendStart()
{
    qDebug() << "===================START";
    for (int i = 0; i < sockets.size(); i++){
        sockets[i]->SendStart();
    }
}

void SoundServer::SendData( const void *data, int len )
{
    qDebug() << "===================SEND";
    for (int i = 0; i < sockets.size(); i++){
        sockets[i]->SendData(data,len);
    }
}

void SoundServer::SendEnd()
{
    qDebug() << "===================END";
    for (int i = 0; i < sockets.size(); i++){
        sockets[i]->SendEnd();
    }
}

void SoundServer::SendCancel()
{
    qDebug() << "===================CANCEL";
    for (int i = 0; i < sockets.size(); i++){
        sockets[i]->SendCancel();
    }
}

void SoundServer::SendKeyPressed( int val )
{
    qDebug() << "===================KEY";
    for (int i = 0; i < sockets.size(); i++){
        sockets[i]->SendKeyPressed(val);
    }
}

void SoundServer::SendEpdOn()
{
    qDebug() << "===================EPDON";
    for (int i = 0; i < sockets.size(); i++){
        sockets[i]->SendEpdOn();
    }
}

void SoundServer::SendEpdOff()
{
    qDebug() << "===================EPDOFF";
    for (int i = 0; i < sockets.size(); i++){
        sockets[i]->SendEpdOff();
    }
}

