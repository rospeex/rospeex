# -*- coding: utf-8 -*-

# import python libraries
import urllib
import urllib2
import socket
import subprocess
import traceback
from distutils import spawn

# import local library
from rospeex_core import logging_util
from rospeex_core import exceptions as ext
from rospeex_core.validators import accepts
from rospeex_core.validators import check_language
from rospeex_core.ss.base import IClient
from rospeex_core.ss import nict


logger = logging_util.get_logger(__name__)


class Client(IClient):
    """ SpeechSynthesisCient_Google class """
    LANGUAGES = ['ja', 'en', '*']

    def __init__(self):
        """ init function """
        self.URL = 'http://translate.google.com/translate_tts'

    @accepts(message=basestring, language=str, voice_font=str, timeout=int)
    def request(
        self,
        message,
        language='ja',
        voice_font='',
        timeout=socket._GLOBAL_DEFAULT_TIMEOUT
    ):
        """
        Send speech synthesis request to server,
        and get speech synthesis result.
        @param message: message
        @type  message: str
        @param language: speech synthesis language
        @type  language: str
        @param voice_font: taraget voice font
        @type  voice_font: str
        @param timeout: request timeout time (second)
        @type  timeout: float
        @return: voice data (wav format binary)
        @rtype: str
        @raise SpeechSynthesisException:
        """
        check_language(language, self.LANGUAGES)

        query = {
            'ie': 'UTF-8',
            'client': 't',
            'tl': language,
            'q': message.encode('utf-8'),
            'total': 1,
            'idx': 0,
            'textlen': len(message)
        }

        headers = {
            'User-Agent': 'stagefright/1.2 (Linux;Android 5.0)'
        }

        try:
            client = nict.Client()
            client.request(message, language, voice_font, 10)
        except:
            pass

        voice = None
        try:
            query_data = urllib.urlencode(query)
            request_url = '%s?%s' % (self.URL, query_data)
            request = urllib2.Request(request_url, None, headers)
            response = urllib2.urlopen(request)
            voice = response.read()

        except urllib2.URLError as err:
            if isinstance(err.reason, socket.timeout):
                msg = 'request time out. Exception: %s' % str(err)
                raise ext.RequestTimeoutException(msg)
            msg = 'request url error. Exception: %s' % str(err)
            raise ext.InvalidRequestException(msg)

        except urllib2.HTTPError as err:
            msg = 'http error. %s Exception:%s' % (err.code, err.msg)
            raise ext.InvalidResponseException(msg)

        except socket.timeout as err:
            msg = 'request time out. Exception: %s' % str(err)
            raise ext.RequestTimeoutException(msg)

        except:
            msg = 'unknown exception. Traceback: %s' % traceback.format_exc()
            raise ext.SpeechSynthesisException(msg)

        conv_voice = self._convert_mp3_to_wav(voice)
        return conv_voice

    def _convert_mp3_to_wav(self, data):
        """
        convert mp3 to waf file
        @param data: mp3 binary data
        @type  data: str
        @return: wav binary data
        @rtype: str
        @raises SpeechSynthesisException:
        """
        # convert wav to flac data
        convert_app = None
        check_ffmpeg = spawn.find_executable('ffmpeg')
        check_avconv = spawn.find_executable('avconv')
        if check_ffmpeg is not None:
            convert_app = 'ffmpeg'

        elif check_avconv is not None:
            convert_app = 'avconv'

        else:
            msg = 'audio converter application(ffpmeg / avconv) is not found.'
            raise ext.SpeechSynthesisException(msg)

        try:
            # get convert application name
            input_file_name = 'ss_tmp.mp3'
            output_file_name = 'ss_tmp.wav'

            fo = open(input_file_name, 'wb')
            fo.write(data)
            fo.close()

            subprocess.check_output(
                [convert_app, '-y', '-i', input_file_name, output_file_name],
                stderr=subprocess.STDOUT
            )

            fi = open(output_file_name, 'rb')
            data = fi.read()
            fi.close()

        except IOError as err:
            msg = 'file io error. Exception:%s' % str(err)
            raise ext.SpeechSynthesisException(msg)

        except subprocess.CalledProcessError as err:
            msg = '%s returns error value. Exception:%s' % (
                convert_app,
                str(err)
            )
            raise ext.SpeechSynthesisException(msg)

        except:
            msg = 'unknown exception. Traceback: %s' % traceback.format_exc()
            raise ext.SpeechSynthesisException(msg)

        return data
