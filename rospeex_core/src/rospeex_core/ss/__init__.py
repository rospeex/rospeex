# -*- coding: utf-8 -*-

from factory import SSEngine as Engine
from factory import SpeechSynthesisFactory as Factory

__all__ = [
    'Engine',
    'Factory'
]
