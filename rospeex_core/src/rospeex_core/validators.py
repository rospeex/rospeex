#!/usr/bin/env python
# -*- coding: utf-8 -*-


import wave
import StringIO
from functools import wraps
from exceptions import ParameterException
from exceptions import UnsupportedLanguageException
from exceptions import InvalidAudioDataException


def accepts(**types):
    """ check function argument types

    @param types: parameter types (kwdict)
    @type  types: dict
    @raise ParameterException: parameter type exception
    """
    def check_accepts(f):
        @wraps(f)
        def wrapper(*args, **kwds):
            for cnt, val in enumerate(args):
                value_type = f.func_code.co_varnames[cnt]
                if value_type in types and \
                        not isinstance(val, types[value_type]):
                    raise ParameterException(
                        "argument '%s'=%r does not match %s" % (
                            value_type,
                            val,
                            types[value_type]
                        )
                    )
                    del types[f.func_code.co_varnames[cnt]]

            for key, val in kwds.iteritems():
                if key in types and not isinstance(val, types[key]):
                    raise ParameterException(
                        "argument '%s'=%r does not match %s" % (
                            key,
                            val,
                            types[key]
                        )
                    )
            return f(*args, **kwds)
        return wrapper
    return check_accepts


def check_wave_data(data, framerate, nchannel, sampwidth, maxlength):
    """ check wave file error.

    @param data: parameter data
    @type  data: str
    @param framerate: wave file framerate
    @type  framerate: int
    @param nchannel: wave file channel
    @type  nchannel: int
    @param sampwidth: sampling width (1=>8bit, 2=>16bit, 4=>32bit)
    @type  sampwidth: int
    @param maxlength: max wave length [ms]
    @type  maxlength: int
    @raises InvalidAudioDataException
    """
    try:
        wav_buffer = StringIO.StringIO(data)
        wav_file = wave.open(wav_buffer, 'rb')
        framerate_ = wav_file.getframerate()
        sampwidth_ = wav_file.getsampwidth()
        channels_ = wav_file.getnchannels()
        nframes_ = wav_file.getnframes()

        read_data = wav_file.readframes(nframes_)
        read_data_size = len(read_data)
        header_data_size = sampwidth_ * channels_ * nframes_
        wave_length_ = int(
            float(wav_file.getnframes()) / wav_file.getframerate() * 1000.0
        )

        if framerate_ != framerate:
            msg = 'the wav file frame rate MUST be %d. curr: %d' % (
                framerate,
                framerate_
            )
            raise InvalidAudioDataException(msg)

        if sampwidth_ != sampwidth:
            msg = 'the wav file samplewidth MUST be %dbit. curr: %d' % (
                sampwidth*8,
                sampwidth_*8
            )
            raise InvalidAudioDataException(msg)

        if channels_ != nchannel:
            msg = 'the wav file channels MUST be %d. curr: %d' % (
                nchannel,
                channels_
            )
            raise InvalidAudioDataException(msg)

        if header_data_size != read_data_size:
            msg = 'the wav file is broken. data_size(by header): '\
                  '%d data_size(by data): %d' % (
                        header_data_size,
                        read_data_size
                    )
            raise InvalidAudioDataException(msg)

        if wave_length_ > maxlength:
            msg = 'the playing time of the wav file is too long. '\
                  'max %d[ms] curr: %d[ms]' % (
                        maxlength,
                        wave_length_
                    )
            raise InvalidAudioDataException(msg)

        if wave_length_ == 0:
            msg = 'the playing time of the wav file is zero.'
            raise InvalidAudioDataException(msg)

    except wave.Error as err:
        msg = 'the input audio data MUST be wav format. '\
              'Exception: %s' % str(err)
        raise InvalidAudioDataException(msg)


def check_language(language, supported_languages):
    """ check supported language

    @param param_name: check parameter name
    @type  param_name: str
    @param languages: supported languages in the client
    @type  languages: list
    @raises UnsupportedLanguageException
    """
    # check supported language
    if language not in supported_languages:
        msg = '%s is unsupported language. supported languages are %s' % (
            language,
            supported_languages
        )
        raise UnsupportedLanguageException(msg)
