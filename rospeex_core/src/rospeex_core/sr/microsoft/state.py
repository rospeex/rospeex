# -*- coding: utf-8 -*-

# python libraries
import StringIO
import wave

# local libraries
from rospeex_core import logging_util
from rospeex_core.sr.base import IState
from rospeex_core.sr.base import PacketType
from rospeex_core.sr.base import SessionState
from sync_client import SyncClient


# get logger
logger = logging_util.get_logger(__name__)


class InitState(IState):
    def __init__(self, api_key, language):
        self._api_key = api_key
        self._language = language

    def run(self, packet_data):
        pass    # pragma: no cover

    def next(self, packet_type):
        if packet_type == PacketType.START:
            logger.debug('Change session state INIT -> START')
            return StartState(self._api_key, self._language)

        else:
            logger.debug('Change session state INIT -> ERROR')
            return ErrorState(self._api_key, self._language)

    def result(self):
        return None     # pragma: no cover

    def state(self):
        return SessionState.INIT


class StartState(IState):
    def __init__(self, api_key, language):
        self._api_key = api_key
        self._language = language

    def run(self, packet_data):
        pass

    def next(self, packet_type):
        if packet_type == PacketType.DATA:
            logger.debug('Change session state START -> DATA')
            return DataState(self._api_key, self._language, None)

        elif packet_type == PacketType.END:
            logger.debug('Change session state START -> END')
            return EndState(self._api_key, self._language, None)

        else:
            logger.debug('Change session state START -> ERROR')
            return ErrorState(self._api_key, self._language)

    def result(self):
        return None     # pragma: no cover

    def state(self):
        return SessionState.START


class DataState(IState):
    def __init__(self, api_key, language, data):
        self._api_key = api_key
        self._language = language
        self._data = data

    def run(self, packet_data):
        if self._data is None:
            self._data = packet_data
        else:
            self._data += packet_data

    def next(self, packet_type):
        if packet_type == PacketType.DATA:
            logger.debug('Change session state DATA -> DATA')
            return DataState(self._api_key, self._language, self._data)

        elif packet_type == PacketType.END:
            logger.debug('Change session state DATA -> END')
            return EndState(self._api_key, self._language, self._data)

        else:
            logger.debug('Change session state DATA -> ERROR')
            return ErrorState(self._api_key, self._language)

    def result(self):
        return None     # pragma: no cover

    def state(self):
        return SessionState.DATA


class EndState(IState):
    def __init__(self, api_key, language, data):
        self._api_key = api_key
        self._language = language
        self._data = data
        self._result_text = ''

    def run(self, packet_data):
        # create wav format data
        output = StringIO.StringIO()
        wav_data = wave.open(output, 'wb')
        wav_data.setnchannels(SyncClient.CHANNELS)
        wav_data.setframerate(SyncClient.FRAMERATE)
        wav_data.setsampwidth(SyncClient.SAMPWIDTH)
        wav_data.writeframes(self._data)

        # send data to client
        client = SyncClient(self._api_key)
        self._result_text = client.request(output.getvalue(), self._language)

    def next(self, packet_type):
        if packet_type == PacketType.START:
            logger.debug('Change session state END -> START')
            return StartState(self._api_key, self._language)

        else:
            logger.debug('Change session state END -> ERROR')
            return ErrorState(self._api_key, self._language)

    def result(self):
        return self._result_text

    def state(self):
        return SessionState.END


class ErrorState(IState):
    def __init__(self, api_key, language):
        self._api_key = api_key
        self._language = language

    def run(self, packet_data):
        pass

    def next(self, packet_type):
        if packet_type == PacketType.START:
            logger.debug('Change session state ERROR -> START')
            return StartState(self._api_key, self._language)

        else:
            logger.debug('Change session state ERROR -> ERROR')
            return ErrorState(self._api_key, self._language)

    def result(self):
        return None     # pragma: no cover

    def state(self):
        return SessionState.ERROR
