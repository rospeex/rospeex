#!-*- coding: utf-8 -*-

# python libraries
import threading

# local libraries
from rospeex_core import logging_util
from rospeex_core.sr.base import Session
from rospeex_core.sr.base import ThreadPool
from sync_client import SyncClient
from state import InitState


logger = logging_util.get_logger(__name__)


class StreamingClient(SyncClient):
    """ Audio Processor class """
    HOST = 'rospeex.nict.go.jp'
    BASE_URL = 'http://%s/nauth/SocketServlet' % HOST
    SEND_THREAD_NUM = 2

    def __init__(self, language='ja', *args, **kwargs):
        super(StreamingClient, self).__init__()
        self._streaming_cb_lock = threading.Lock()
        self._streaming_cb_list = []

        # start session
        self._pool = ThreadPool(self.SEND_THREAD_NUM)
        self._session = Session(
            InitState(
                self.BASE_URL,
                language,
                self._pool
            )
        )
        self._session.start()
        self._streaming_state = self._session.state()

    def support_streaming(self):
        """
        check support streaming
        @returns: True for support streaming / False for NOT support streaming
        """
        return True

    def set_streaming_config(self, language):
        """ set streaming config
        @param language:
        """
        state = InitState(self.BASE_URL, language, self._pool)
        self._session.set_next_state(state)

    def add_streaming_packet(self, packet_type, packet_data):
        """ add streaming packet
        @param packet_type:
        @type  packet_type: int
        @param packet_data:
        @param packet_data: str
        """
        self._session.add_packet(packet_type, packet_data)

    def register_streaming_cb(self, cb):
        """
        register streaming result callback
        @param cb:
        @type cb:
        """
        self._session.register_result_cb(cb)

    def unregister_streaming_cb(self, cb):
        """
        unregister streaming result callback
        @param cb:
        @type cb:
        """
        self._session.register_unresult_cb(cb)

    def join(self, timeout=None):
        """
        join streaming client
        @param timeout:
        @type timeout:
        """
        self._pool.end()
        self._session.join(timeout)
