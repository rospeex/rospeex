# !/usr/bin/env python
# !-*- coding: utf-8 -*-

# import python libraries
import email

# local library
import stml_type


__all__ = ['MIMEMessage']


class MIMEMessage(object):
    @classmethod
    def create_http_header(cls, cookie=None):
        headers = {
            'Content-Type': 'application/x-www-form-urlencoded',
            'Cache-Control': 'no-cache',
            'pragma': 'no-cache',
        }

        # add cookie data to http header
        if cookie is not None:
            headers['cookie'] = cookie
        return headers

    @classmethod
    def create_header_request(cls, language, max_nbest):
        stml_str = cls._create_stml(language, max_nbest)
        boundary = cls._create_mime_boundary_string()
        message = cls._create_mime_multipart(boundary)
        message += "--{}\r\n".format(boundary)
        message += cls._create_mime_text(stml_str)
        message += "--{}--\r\n".format(boundary)
        return message

    @classmethod
    def create_finish_request(cls):
        boundary = cls._create_mime_boundary_string()
        message = cls._create_mime_multipart(boundary)
        message += "--{}\r\n".format(boundary)
        message += cls._create_mime_text('')
        message += "--{}--\r\n".format(boundary)
        return message

    @classmethod
    def create_data_request(cls, data):
        boundary = cls._create_mime_boundary_string()
        message = cls._create_mime_multipart(boundary)
        message += "--{}\r\n".format(boundary)
        message += cls._create_mime_text('')
        message += "--{}\r\n".format(boundary)
        message += cls._create_mime_binary(data)
        message += "--{}--".format(boundary)
        return message

    @classmethod
    def create_bundle_request(cls, stml_str, data):
        boundary = cls._create_mime_boundary_string()
        message = cls._create_mime_multipart(boundary)
        message += "--{}\r\n".format(boundary)
        message += cls._create_mime_text(stml_str)
        message += "--{}\r\n".format(boundary)
        message += cls._create_mime_binary(data)
        message += "--{}--".format(boundary)
        return message

    @classmethod
    def _create_stml(cls, language, max_nbest):
        """ create stml string
        """
        form = stml_type.STMLType()
        form.add_version('1.1')
        form.add_utterance_id('10')

        user_form = stml_type.UserFormat()
        user_form.add_id('S2TDU0100A0000UU0150NICTS2SSample-Unknown')
        form.add_user(user_form)

        sr_in = stml_type.SRInputType()
        sr_in.add_language(language)
        sr_in.add_domain('Travel')
        sr_in.add_max_nbest(str(max_nbest))

        voice = stml_type.VoiceFormat()
        voice.add_id('INPUT')
        sr_in.add_voice(voice)

        output_form = stml_type.OutputTextFormat()
        output_form.add_form('SurfaceForm')
        sr_in.add_output_text_format(output_form)

        # Endian : Little
        # Audio Format : RAW
        input_form = stml_type.InputAudioFormat()
        input_form.add_endian('Little')
        input_form.add_audio('RAW')
        sr_in.add_input_audio_format(input_form)
        form.add_sr_in(sr_in)
        return form.tostring()

    @classmethod
    def _create_mime_boundary_string(cls):
        """ create mime multipart boundary string
        """
        boundary = "--ohgaohdaghahdfaldkhf"
        return boundary

    @classmethod
    def _create_mime_multipart(cls, boundary_str):
        """ create mime mutipart string
        """
        message = "Message-ID: {}\r\n".format(email.utils.make_msgid())
        message += "MIME-Version: 1.0\r\n"
        message += "Content-Type: multipart/mixed;\r\n"
        message += "\tboundary=\"{}\"\r\n\r\n".format(boundary_str)
        return message

    @classmethod
    def _create_mime_text(cls, text=''):
        """ create mime text string

        @param text:
        @return: mime text
        """
        message = "Content-Type: text/xml;charset=utf-8\r\n"
        message += "Content-Transfer-Encoding: 7bit\r\n\r\n"
        message += text
        message += "\r\n"
        return message

    @classmethod
    def _create_mime_binary(cls, audio_data):
        """ create mime binary part

        @param audio_data:
        @param return: mime message
        """
        message = "Content-Type: application/octet-stream\r\n"
        message += "Content-Transfer-Encoding: binary\r\n\r\n"
        message += audio_data
        message += "\r\n"
        return message
