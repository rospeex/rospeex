#!/bin/bash
#
# Script to set the microphone gain
#
# set default prime sense microphone volume to 65%
# if you change other microphones, 
# you can get lists as follows
#  $ pactl list sources short

is_pactl_installed=1
which pactl
is_pactl_installed=$?

if [ ${is_pactl_installed} = 0 ]; then
    dev_list=`pactl list sources short | grep "alsa_input.usb-PrimeSense_PrimeSense_Device-[0-9]\{2\}-Device.analog-stereo" | cut -f 2`
    
    for device in ${dev_list}; do
        echo "set microphone \"${device}\" gain to 65%"
        pactl set-source-volume ${device} 65%
    done
else
    echo "Pactl is not installed. Stop setting microphone gain."
    exit
fi
