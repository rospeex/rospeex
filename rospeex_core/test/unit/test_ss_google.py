#!/usr/bin/env python
# -*- coding: utf-8 -*-

# import python libraries
import os
import logging
import unittest
from nose.tools import raises
from nose.tools import nottest
import ConfigParser

from rospeex_core import exceptions as ext
from rospeex_core.ss.google import Client

# setup logging
FORMAT = '[%(levelname)s] %(message)s @%(filename)s:%(funcName)s:%(lineno)d'
logging.basicConfig(format=FORMAT, level=logging.DEBUG)
logger = logging.getLogger(__name__)


class TestGoogleClient(unittest.TestCase):
    def setUp(self):
        base_dir = os.path.dirname(__file__)
        settings = ConfigParser.ConfigParser()
        filename = os.path.join(base_dir, 'config.cfg')
        settings.read(filename)
        self.big_text_file = os.path.join(
            base_dir,
            settings.get('SpeechSynthesis', 'big_text_for_google_file')
        )

        self.big_text_data = None
        with open(self.big_text_file, 'r') as f:
            self.big_text_data = f.read()

    @raises(ext.ParameterException)
    def test_request_invalid_message(self):
        message = None
        language = 'ja'
        voice_font = '*'
        timeout = 100
        client = Client()
        client.request(message, language, voice_font, timeout)

    @raises(ext.UnsupportedLanguageException)
    def test_request_invalid_language(self):
        message = 'hello'
        language = 'de'        # deutsch
        voice_font = '*'
        timeout = 100
        client = Client()
        client.request(message, language, voice_font, timeout)

    @raises(ext.InvalidRequestException)
    def test_request_invalid_url(self):
        message = 'hello'
        language = 'en'
        voice_font = '*'
        timeout = 100
        client = Client()
        client.URL = 'http://translate.google.com/translate_tts2?'
        client.request(message, language, voice_font, timeout)

    @nottest
    @raises(ext.SpeechSynthesisException)
    def test_request_invalid_voice_font(self):
        message = 'hello'
        language = 'ja'
        voice_font = 'hogehoge'
        timeout = 100
        client = Client()
        client.request(message, language, voice_font, timeout)

    @nottest
    @raises(ext.RequestTimeoutException)
    def test_request_server_timeout_prev(self):
        message = self.big_text_data
        language = 'en'
        voice_font = '*'
        timeout = 1
        client = Client()
        client.request(message, language, voice_font, timeout)

    @nottest
    @raises(ext.RequestTimeoutException)
    def test_request_server_timeout_post(self):
        message = self.big_text_data
        language = 'en'
        voice_font = '*'
        timeout = 2
        client = Client()
        client.request(message, language, voice_font, timeout)

    @nottest
    def test_request_valid_big_message(self):
        message = self.big_text_data        # alice in wonder land
        language = 'en'
        voice_font = '*'
        timeout = 100
        client = Client()
        client.request(message, language, voice_font, timeout)

    @nottest
    def test_request_valid_japanese_message(self):
        message = u'こんにちは'
        language = 'ja'
        voice_font = '*'
        timeout = 100
        client = Client()
        client.request(message, language, voice_font, timeout)

    @nottest
    def test_request_valid_english_message(self):
        message = 'hello everyone.'
        language = 'en'
        voice_font = '*'
        timeout = 100
        client = Client()
        client.request(message, language, voice_font, timeout)


if __name__ == '__main__':
    import rosunit
    test_class = TestGoogleClient
    rosunit.unitrun(
        'rospeex_core',
        'speech_synthesis_client_google',
        test_class,
        None,
        coverage_packages=['rospeex_core.ss']
    )
