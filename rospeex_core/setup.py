## ! DO NOT MANUALLY INVOKE THIS setup.py, USE CATKIN INSTEAD

from setuptools import setup
from catkin_pkg.python_setup import generate_distutils_setup

setup_args = generate_distutils_setup(
    packages=[
        'rospeex_core',
        'rospeex_core.spi',
        'rospeex_core.sr',
        'rospeex_core.sr.base',
        'rospeex_core.sr.google',
        'rospeex_core.sr.microsoft',
        'rospeex_core.sr.nict',
        'rospeex_core.ss'
    ],
    package_dir={'': 'src'}
)

setup(**setup_args)
