#!/usr/bin/env python
# -*- coding: utf-8 -*-

import rospy
import datetime
import re

# Import other libraries
from rospeex_if import ROSpeexInterface


class Demo(object):
    """ Demo class """
    def __init__(self):
        """ Initializer """
        self._interface = ROSpeexInterface()

    def sr_response(self, message):
        """ speech recognition response callback
        @param message: recognize result (e.g. what time is it ?)
        @type  message: str
        """
        time = re.compile('(?P<time>何時)').search(message)

        print 'you said : %s' % message
        if time is not None:
            d = datetime.datetime.today()
            text = u'%d時%d分です。' % (d.hour, d.minute)

            # rospeex reply
            print 'rospeex reply : %s' % text
            self._interface.say(text, 'ja', 'nict')

    def run(self):
        """ run ros node """
        # initialize ros node
        rospy.init_node('ss_sr_demo')

        # initialize rospeex
        self._interface.init()
        self._interface.register_sr_response(self.sr_response)
        self._interface.set_spi_config(language='ja', engine='nict')
        rospy.spin()

if __name__ == '__main__':
    try:
        node = Demo()
        node.run()
    except rospy.ROSInterruptException:
        pass
