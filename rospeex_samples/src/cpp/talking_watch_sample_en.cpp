#include <iostream>
#include <sstream>
#include <boost/regex.hpp>
#include <boost/date_time/posix_time/posix_time.hpp>
#include <ros/ros.h>
#include "rospeex_if/rospeex.h"

static rospeex::Interface interface;

void sr_response( const std::string& msg )
{
	using boost::posix_time::ptime;
	using boost::posix_time::second_clock;
	std::string msg_tr(msg);

	std::cerr << "you said : " << msg << std::endl;
	std::transform(msg_tr.begin(), msg_tr.end(), msg_tr.begin(), ::tolower);

	boost::regex time_reg(".*what time.+");
	if ( boost::regex_match(msg_tr, time_reg) ) {
		std::string text = "";
		ptime now = second_clock::local_time();
		std::stringstream ss;
		ss << "It's " << now.time_of_day().hours() << ":" << now.time_of_day().minutes() << ".";
		text = ss.str();
		std::cerr << "rospeex reply : " << text << std::endl;
		interface.say(text, "en", "nict");
	}
}

int main( int argc, char** argv )
{
	ros::init(argc, argv, "sr_ss_demo");

	interface.init();
	interface.registerSRResponse( sr_response );
	interface.setSPIConfig("en", "nict");
	ros::spin();
	return 0;
}
