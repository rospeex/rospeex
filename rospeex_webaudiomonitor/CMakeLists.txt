cmake_minimum_required(VERSION 2.8.3)
project(rospeex_webaudiomonitor)

find_package(catkin REQUIRED COMPONENTS
  rospy
)

catkin_python_setup()

catkin_package(
  LIBRARIES
  CATKIN_DEPENDS rospy
  DEPENDS
)

catkin_install_python(PROGRAMS
  scripts/server.py
  scripts/webaudiomonitor_server.py
  DESTINATION ${CATKIN_PACKAGE_BIN_DESTINATION}
)

install(DIRECTORY www/
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/www
  PATTERN ".svn" EXCLUDE
)

install(DIRECTORY launch/
  DESTINATION ${CATKIN_PACKAGE_SHARE_DESTINATION}/launch
  PATTERN ".svn" EXCLUDE
)

