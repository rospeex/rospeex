/**
 * constructor
 * @param {function} connected_cb: connected callback function.
 * @param {function} disconnected_cb: disconnected callback function.
 * @param {function} message_cb: message callback function. this function is called when receive vad packet.
 * @param {function} epd_cb: epd state changed callback. this function is called when epd on / off packet received.
 * @param {function} ss_text_cb:
 * @param {function} sr_text_cb:
 */
var WsClient = function(connected_cb, disconnected_cb, message_cb, epd_cb, ss_text_cb, sr_text_cb)
{
  this.connected = false;
  this.ws = null;
  this.connected_cb = connected_cb;
  this.disconnected_cb = disconnected_cb;
  this.message_cb = message_cb;
  this.epd_cb = epd_cb;
  this.ss_text_cb = ss_text_cb;
  this.sr_text_cb = sr_text_cb;
  this.timeout_count = 0;
  this.EPD_STATE_CHANGED_DELAY = 1000;
}


/**
 * connect to ws server
 */
WsClient.prototype.connect = function(ip, port)
{
  // console.log('connect to client ' + ip + ':' + port);

  // create web socket
  this.ws = new WebSocket("ws://" + ip + ":" + port + "/websocket");
  this.ws.binaryType = "arraybuffer";

  // on open function
  this.ws.onopen = this.onopen.bind(this);

  // on close function
  this.ws.onclose = this.onclose.bind(this);

  // on message function
  this.ws.onmessage = this.onmessage.bind(this);

  // on error function
  this.ws.onerror = this.onerror.bind(this);
};


/**
 * open connection
 */
WsClient.prototype.onopen = function()
{
  console.log('websocket client is opened.');
  this.connected = true;
};


/**
 * on close
 * @param  {[type]} e: what
 */
WsClient.prototype.onclose = function(e)
{
  console.log('websocket client is closed.');
  this.disconnect();
};


/**
 * [onerror description]
 * @param  {[type]} e
 * @return {[type]}
 */
WsClient.prototype.onerror = function(e)
{
  console.log('on error');
};


WsClient.prototype.convertBinArray2UtfString = function(data) {
  var encoded = String.fromCharCode.apply(null, data);
  var decoded = decodeURIComponent(escape(encoded));
  return decoded;
};

/**
 * on message
 * @param  {[type]} e
 */
WsClient.prototype.onmessage = function(e)
{
  if (e.data instanceof ArrayBuffer) {

    var data = new Uint8Array(e.data);
    var head = data[0];

    if(head == VAD_HEADER){
      var recv_data = this.parseFrame(data);
      this.message_cb(recv_data.type, recv_data.frame);
      recv_data = null;

    } else if (head == SS_TEXT) {
      data_frame = data.subarray(1);
      text = this.convertBinArray2UtfString(data_frame)
      this.ss_text_cb(text);
      console.log('ss_text received. ' + text);

    } else if (head == SR_TEXT) {
      data_frame = data.subarray(1);
      text = this.convertBinArray2UtfString(data_frame)
      this.sr_text_cb(text);
      console.log('sr_text received. ' + text);

    } else {
      console.log('invalid header [%d]', head)
    }

    data = null;

  } else {
    // console.log("%%%%% text data received : " +e.data);
    if(e.data == "[VAD SERVER Connected]"){
      this.connected_cb();
      console.log('vad connected.');

    } else if (e.data == "[VAD SERVER Disconnected]"){
      this.disconnect();
      console.log('vad server disconnected.');

    } else if (e.data.indexOf("[SS] epdoff") >= 0 ){
      var me = this;
      setTimeout(function() { me.epd_cb(false);}, this.EPD_STATE_CHANGED_DELAY);
      console.log('epd off.');

    } else if (e.data.indexOf("[SS] epdon") >= 0 ){
      var me = this;
      setTimeout(function() { me.epd_cb(true);}, this.EPD_STATE_CHANGED_DELAY);
      console.log('epd on.');

    } else {
      console.log('unknown frame [%s]', e.data);
    }
  }

};


/**
 * disconnect to server
 */
WsClient.prototype.disconnect = function()
{
  if ( this.isConnected() ) {
    this.disconnected_cb();
    this.ws.close();
    delete this.ws;
    this.ws = null;
    this.connected = false;
  }
};


/**
 * check connection
 * @return {Boolean} true for connected / false for disconnected.
 */
WsClient.prototype.isConnected = function()
{
  return this.connected;
};


/**
 * parse vad frame
 * @param  {Uint8Array} data: data frame
 * @return {number} type: vad type
 * @return {Uint8Array}: vad data frame
 */
WsClient.prototype.parseFrame = function(data)
{
  var receivedType;
  var type_offset = WEBSOCKET_HEADER_SIZE + VAD_FRAME_HEADER_SIZE;
  receivedType = data[type_offset - 1];
  frame = data.subarray(type_offset);
  return {
    type: receivedType,
    frame: frame
  };
};


/**
 * send binary data
 * @param  {Uint8Array} _data: send data
 */
WsClient.prototype.send = function(_data)
{
  try {
    if ( this.isConnected() ) {
      this.ws.send(_data);
    }

  } catch ( e ) {
    console.log(e);
  }
};

