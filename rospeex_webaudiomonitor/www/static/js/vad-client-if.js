var VAD_HEADER = 22;
var VAD_TOF   = 9;
var VAD_START = 1;
var VAD_DATA  = 0;
var VAD_END   = 2;
var VAD_CANCEL= 6;
var VAD_EOF   = 10;
var SS_TEXT = 30;
var SR_TEXT = 31;

var WEBSOCKET_HEADER_SIZE = 1;
var VAD_FRAME_HEADER_SIZE = 4;
var VAD_FRAME_CMD_SIZE = 320;
var VAD_CMD_FRAME_SIZE = WEBSOCKET_HEADER_SIZE + VAD_FRAME_HEADER_SIZE + VAD_FRAME_CMD_SIZE;

var VAD_FRAME_DATA_SIZE = 320;
var VAD_DATA_FRAME_SIZE = WEBSOCKET_HEADER_SIZE + VAD_FRAME_HEADER_SIZE + VAD_FRAME_DATA_SIZE;

var VAD_SEND_BUF_FRAME_COUNT = 100;
var VAD_SEND_BUF_SIZE = VAD_CMD_FRAME_SIZE * VAD_SEND_BUF_FRAME_COUNT;
var VAD_RECV_BUF_FRAME_COUNT = 100;
var VAD_RECV_BUF_SIZE = VAD_CMD_FRAME_SIZE * VAD_RECV_BUF_FRAME_COUNT;

var VAD_DATA_BUFNUM = 1000;
var VAD_DATA_SIZE = VAD_FRAME_DATA_SIZE / 2;


/**
 * constructor
 * @param {function} connected_cb: connected callback function.
 * @param {function} disconnected_cb: disconnected callback funciton.
 * @param {function} vad_state_changed_cb: vad state changed callback function.
 * @param {function} epd_state_changed_cb: epd state changed callback function.
 */
var VadClient = function(connected_cb, disconnected_cb, vad_state_changed_cb, epd_state_changed_cb, vad_datasize, ss_text_cb, sr_text_cb)
{
  this.ws_client = null;
  this.vad_state_changed_cb = vad_state_changed_cb;
  this.epd_state_changed_cb = epd_state_changed_cb;
  this.connected_cb = connected_cb;
  this.disconnected_cb = disconnected_cb;
  this.vad_datasize = vad_datasize;
  this.ss_text_cb = ss_text_cb
  this.sr_text_cb = sr_text_cb

  this.curIndexSendBuf = VAD_FRAME_HEADER_SIZE;
  this.curIndexRecvBuf = 0;
  this.sendFrameCount = 0;
  this.recvFrameCount = 0;
  this.connected = false;
  this.sendOffset = 0;
  this.monitableVadData = [];
  this.epdState = true;

  this._sendFrame = new Uint8Array(VAD_CMD_FRAME_SIZE);
  this._sendDataFrame = new Uint8Array(VAD_DATA_FRAME_SIZE);
  this._sendBuf = new Uint8Array(VAD_SEND_BUF_SIZE + VAD_CMD_FRAME_SIZE);
  this._recvBuf = new Uint8Array(VAD_RECV_BUF_SIZE);

  this.vadData = [];
  this.vadDataReceived = false;
  this.vadDataActiveBuf = -1;
  this.vadDataActiveOffset = 0;
  this.sendable = false;
  this.vadEnableCount = 0;
};


/**
 * connect vad client
 * @param {string} ip: ws server ip address.
 * @param {string} port: ws server port.
 */
VadClient.prototype.connect = function(ip, port)
{
  this.disconnect();

  // create new connection
  this.ws_client = new WsClient(
    this.vadConnected.bind(this),
    this.vadDisconnected.bind(this),
    this.receiveData.bind(this),
    this.epdStateChanged.bind(this),
    this.ss_text_cb,
    this.sr_text_cb
  );
  this.ws_client.connect(ip, port);
};


/**
 * disconnect vad client.
 */
VadClient.prototype.disconnect = function()
{
  if ( this.ws_client ) {
    this.ws_client.disconnect();
    delete this.ws_client;
    this.ws_client = null;
  }
};


/**
 * initialize send buffer
 */
VadClient.prototype.initSendBuf = function()
{
  curIndexSendBuf = VAD_FRAME_HEADER_SIZE;
};


/**
 * initialize receive buffer
 */
VadClient.prototype.initRecvBuf = function()
{
  curIndexRecvBuf = 0;
};


/**
 * set disconnected state.
 */
VadClient.prototype.setDisConnectedState = function()
{
  this.connected = false;
};


/**
 * set connected state.
 */
VadClient.prototype.setConnectedState = function()
{
  this.connected = true;
  this.initSendBuf();
  this.initRecvBuf();
  this.sendFrameCount = 0;
  this.recvFrameCount = 0;

  for(var i=0;i<VAD_CMD_FRAME_SIZE;i++){
    this._sendFrame[i] = 0;
  }

  this._sendDataFrame[0] = VAD_HEADER;
  for( var i=0; i < VAD_FRAME_HEADER_SIZE; i++ ) {
    this._sendDataFrame[i+WEBSOCKET_HEADER_SIZE] = 0;
  }

  this._sendDataFrame[1] = VAD_DATA;
};


/**
 * check vad connection
 * @return {Boolean} true for connected. / false for disconnected.
 */
VadClient.prototype.isConnected = function()
{
  return this.connected;
};


/**
 * send TOF frame packet.
 */
VadClient.prototype.sendTOF = function()
{
  console.log('### vadClient_sendTOF');
  this.sendFrameCount = 0;
  this.recvFrameCount = 0;
  this.sendFrame(VAD_TOF);
};


/**
 * send EOF frame packet.
 */
VadClient.prototype.sendEOF = function()
{
  console.log('### vadClient_sendEOF');
  this.sendFrame(VAD_EOF);
};


/**
 * send STARTPU frame packet.
 */
VadClient.prototype.sendSTARTPU = function()
{
  console.log('### vadClient_sendSTARTPU');
  this.sendFrame(VAD_START);
};


/**
 * send ENDPU frame packet.
 */
VadClient.prototype.sendENDPU = function()
{
  console.log('### vadClient_sendENDPU');
  this.sendFrame(VAD_END);
};


/**
 * send CANCELPU frame packet.
 */
VadClient.prototype.sendCANCELPU = function()
{
  console.log('### vadClient_sendCANCELPU');
  this.sendFrame(VAD_CANCEL);
};


/**
 * send frame data to ws client.
 * @param  {number} type: vad frame type
 */
VadClient.prototype.sendFrame = function(type)
{
    this._sendFrame[0] = VAD_HEADER;
    this._sendFrame[1] = type;
    this.ws_client.send(this._sendFrame);
    this.sendFrameCount++;
};


/**
 * send data frame to ws client
 * @param  {Array} data
 */
VadClient.prototype.sendDataFrame = function(data)
{
    if ( !this.isConnected() || this.ws_client == null ) {
      return;
    }

    var len = data.length;
    var offset = 0;
    var idx;
    var header_size = WEBSOCKET_HEADER_SIZE + VAD_FRAME_HEADER_SIZE;

    if ( this.sendOffset > 0 ) {
      if ( ( VAD_FRAME_DATA_SIZE - this.sendOffset ) * 2 > len ) {
        for( idx = 0; idx < len; idx++ ) {
          this._sendDataFrame[this.sendOffset + idx + header_size] = data[idx + offset];
        }
        len -= idx;
        this.sendOffset += idx;

      } else {
        for( idx = 0; idx < (VAD_FRAME_DATA_SIZE - this.sendOffset ); idx++ ) {
          this._sendDataFrame[this.sendOffset + idx + header_size] = data[idx + offset];
        }
        this.ws_client.send(this._sendDataFrame);
        this.sendFrameCount++;
        len -= idx;
        offset += idx;
        this.sendOffset = 0;
      }
    }

    for ( ; len >= VAD_FRAME_DATA_SIZE; ) {
      for( idx = 0; idx < VAD_FRAME_DATA_SIZE; idx++ ) {
        this._sendDataFrame[idx + header_size] = data[idx + offset];
      }
      this.ws_client.send(this._sendDataFrame);
      this.sendFrameCount++;
      len -= VAD_FRAME_DATA_SIZE;
      offset += VAD_FRAME_DATA_SIZE;
    }
    if ( len > 0 ) {
      for( var i = 0; i < len; i++ ) {
        this._sendDataFrame[this.sendOffset + i + header_size] = data[i + offset];
      }
      this.sendOffset += len;
    }
};


/**
 * convert vad data packet to monitable data
 * @param  {Array} data
 */
VadClient.prototype.convertDataPacket = function(data)
{
  if ( this.vadDataActiveBuf < 0 ) {
    for (var i = 0; i < VAD_DATA_BUFNUM; i++) {
      this.vadData[i] = new Float32Array(this.vad_datasize);
    }
    this.vadDataActiveBuf = 0;
  }

  if ( data.length != VAD_FRAME_DATA_SIZE ) {
    console.log( '!!!!! Rcv Data : Illegal length ' + data.length );

  } else {

    var idx, idx2;
    var upper, lower;
    var int16_data = new Int16Array(1);
    for ( idx = 0; idx < VAD_DATA_SIZE; idx++ ) {
      idx2 = idx * 2;
      upper = data[ idx2 ];
      lower = data[ idx2 + 1 ];
      lower &= 0x00ff;
      int16_data[0] = upper << 8 ;
      int16_data[0] &= 0xff00;
      int16_data[0] |= lower;
      this.vadData[this.vadDataActiveBuf][this.vadDataActiveOffset + idx] = int16_data[0] / 32767.0;
    }

    if ( this.vadDataActiveOffset + idx >= this.vad_datasize) {

      var tmp_data = new Float32Array(this.vad_datasize);
      tmp_data.set(this.vadData[this.vadDataActiveBuf]);
      // this.monitableVadData.push(this.vadData[this.vadDataActiveBuf]);
      this.monitableVadData.push(tmp_data);
      this.vadDataActiveBuf = ( this.vadDataActiveBuf + 1 ) % VAD_DATA_BUFNUM;
      this.vadDataActiveOffset = 0;

    } else {
      this.vadDataActiveOffset += VAD_DATA_SIZE;
    }
  }
};


/**
 * check vad connection state
 * @return {Boolean}
 */
VadClient.prototype.isCommunicable = function()
{
  return this.isConnected();
};


/**
 * check vad connection state
 * @return {Boolean}
 */
VadClient.prototype.isSendable = function()
{
  return this.sendable;
};


/**
 * vad connected callback function
 * this function called by ws client.
 */
VadClient.prototype.vadConnected = function()
{
  console.log('vadConnected is called.');
  this.setConnectedState();

  this.sendENDPU();
  this.sendEOF();

  this.sendTOF();
  this.sendable = true;

  // call connected callback
  this.connected_cb();
  this.ctrlEPD( this.epdState );
};


/**
 * vad disconnected callback function
 * this function called by ws client.
 * @return {[type]}
 */
VadClient.prototype.vadDisconnected = function()
{
  console.log('vadDisconnected is called.');
  this.sendable = false;
  this.setDisConnectedState();
  console.log( '%%%%% animation restart (stream) %%%%%');
  this.monitableVadData = [];
  this.vadDataActiveBuf = -1;
  this.vadDataActiveOffset = 0;
  this.vadDataReceived = false;

  // call disconnected callback
  this.disconnected_cb();
};


/**
 * get monitable vad data
 * monitable vad data is converted for vad monitor.
 * @return {Array} monitable vad data
 */
VadClient.prototype.getMonitableVadData = function()
{
  if(this.monitableVadData.length > 0) {
    var ret_value = this.monitableVadData.shift();
    return ret_value;
  }
  return null;
};


/**
 * receive ws packet data
 * @param  {number} type: vad packet typelog
 * @param  {Float32Array} data: vad data packet
 */
VadClient.prototype.receiveData = function(type, data)
{
  if ( type == VAD_TOF ) {
    // console.log('TOF packet received');
    this.vad_state_changed_cb( false );

  } else if ( type == VAD_DATA ) {
    this.convertDataPacket(data);

  } else if ( type == VAD_START ) {
    // console.log('START packet received');
    this.vad_state_changed_cb( true );

  } else if ( type == VAD_END ) {
    // console.log('END packet received');
    this.vad_state_changed_cb( false );

  } else if ( type == VAD_CANCEL ) {
    // console.log('CANCEL packet received');
    this.vad_state_changed_cb( false );

  } else {
    // console.log('unknwon packet type ' + type);
  }
};

/**
 * epd state hanged callback
 * @param  {[type]} epd_on_flag [description]
 * @return {[type]}             [description]
 */
VadClient.prototype.epdStateChanged = function(epd_on_flag)
{
  this.epd_state_changed_cb(epd_on_flag);
};

/**
 * set end point detection flag
 * @param  {bool} epd_on_flag: true for epd on / false for epd off
 */
VadClient.prototype.ctrlEPD = function(epd_on_flag)
{
  this.epdState = epd_on_flag;

  if ( this.ws_client != null ) {
    if (epd_on_flag) {
      console.log('send epd on.');
      this.ws_client.send('EPD_ON\n\n');

    } else {
      console.log('send epd off.');
      this.ws_client.send('EPD_OFF\n\n');
    }
  }
};


