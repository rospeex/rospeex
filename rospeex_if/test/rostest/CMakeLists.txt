cmake_minimum_required(VERSION 2.8.3)

if (CATKIN_ENABLE_TESTING)
  find_package(rostest REQUIRED)

  # sr node test (01-01 sync speech recognition)
  add_rostest(tests/01-01.test)

  # sr node test (02-01 sync speech recognition)
  add_rostest(tests/02-01.test)

  # sr node test (03-01 stream speech recognition)
  add_rostest(tests/03-01.test)

  # sr node server stop
  ## add_rostest(tests/04-03_nict_server_stop.test)

  # sr node test (invalid parameter)
  add_rostest(tests/05-01.test)

  # sr node test (invalid file)
  add_rostest(tests/06-01.test)
  add_rostest(tests/06-02.test)
  add_rostest(tests/06-03.test)
  add_rostest(tests/06-04.test)

  # ss node test (text to speech)
  add_rostest(tests/31-01.test)
  add_rostest(tests/32-01.test)
  add_rostest(tests/35-01.test)
  add_rostest(tests/36-01.test)

  # ss node server stop
  ## add_rostest(tests/34-03_nict_server_stop.test)
endif()
