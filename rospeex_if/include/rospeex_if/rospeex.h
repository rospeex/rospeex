#ifndef _ROSPEEX_INTERFACE_H_
#define _ROSPEEX_INTERFACE_H_

#include <string>
#include <boost/shared_ptr.hpp>
#include <boost/function.hpp>
#include <boost/algorithm/string.hpp>

namespace rospeex
{


typedef boost::function<void(const std::string&)> SpeechSynthesisCallback;			//!< speech synthesis calllback type
typedef boost::function<void(const std::string&)> SpeechRecognizeCallback;			//!< speech recognition callback type

/*!
 * @class rospeex Interface class
 */
class Interface
{
public:
	/*!
	 * @brief constructor
	 */
	Interface();

	/*!
	 * @brief destructor
	 */
	virtual ~Interface();

	/*!
	 * @brief Initializing rospeex.
	 * @param[in] ss_enable  set true to enable speech synthesis
	 * @param[in] sr_enable  set true to enable speech recognition
	 * @param[in] spi_enable  set true to enable signal processing interface (waveform monitor)
	 * @return true: succeed, false: failed
	 */
	bool init( bool ss_enable=true, bool sr_enable=true, bool spi_enable=true );

	/*!
	 * @brief Performs speech synthesis, and outputs to the speaker.
	 * @param[in] msg  text to perform speech synthesis
	 * @param[in] language  language setting (ja, en, zh, ko, id, my, th, vi)
	 * @param[in] engine  speech synthesis engine setting (nict or google or voicetext)
	 * (nict: supports ja, en, zh, ko, id, my, th, vi, fr, es; google: supports ja, en)
	 * @param[in] voice_font  voice font setting
     * （nict(ja): F128 or F117,
     * nict(en): EF007,
     * nict(zh): CJF101,
     * nict(ko): KF001,
     * nict(id/my/th/vi): None,
     * google(ja/en): None,
     * voicetext(ja): show, haruka, hikari, takeru, santa, bear）
	 */
	void say( const std::string& msg,
				const std::string& language = "ja",
				const std::string& engine = "nict",
				const std::string& voice_font = "*" );

	/*!
	 * @brief Performs speech synthesis, and outputs to the wave file.
	 * @param[in] msg  text to perform speech synthesis
	 * @param[in] file  filepath to output
	 * @param[in] language  language setting (ja, en, zh, ko, id, my, th, vi)
	 * @param[in] engine  speech synthesis engine setting (nict or google)
	 * (nict: supports ja, en, zh, ko, id, my, th, vi; google: supports ja, en)
	 * @param[in] voice_font  voice font setting
     * （nict(ja): F128 or F117,
     * nict(en): EF007,
     * nict(zh): CJF101,
     * nict(ko): KF001,
     * nict(id/my/th/vi): None,
     * google(ja/en): None,
     * voicetext(ja): show, haruka, hikari, takeru, santa, bear）
	 */
	void tts( const std::string& msg,
				const std::string& file,
				const std::string& language = "ja",
				const std::string& engine = "nict",
				const std::string& voice_font = "*" );

	/*!
	 * @brief Recognizing voice data.
	 * @param[in] data  voice file (wave format: 16kHz, 16bit, mono, LE)
	 * @param[in] language  language setting (ja, en, zh, ko, id, my, th, vi, fr, es)
	 * @param[in] engine  speech recognition engine setting (nict or google)
	 * (nict: supports ja, en, zh, ko, id, my, th, vi, fr, es; google: supports ja, en)
	 */
	void recognize( const std::string& data,
						const std::string& language = "ja",
						const std::string& engine = "nict" );

	/*!
	 * @brief Registering a speech recognition callback function.
	 * @param[in] callback form: boost::function<void(const std::string&)>
	 * std::string: speech recognition result surface string
	 */
	void registerSRResponse( const SpeechRecognizeCallback& callback );

	/*!
	 * @brief Registering a speech recognition callback function.
	 * @param[in] fp form: void(*fp)(const std::string& msg)
	 * msg: speech recognition result surface string
	 */
	void registerSRResponse( void(*fp)(const std::string& msg) );

	/*!
	 * @brief Registering a speech synthesis callback function.
	 * @param[in] callback form: boost::function<void(const std::string&)>
	 * std::string: speech synthesis result audio file (wave)
	 */
	void registerSSResponse( const SpeechSynthesisCallback& callback );

	/*!
	 * @brief Registering a speech synthesis callback function.
	 * @param[in] fp form: void(*fp)(const std::string& msg)
 	 * data: speech synthesis result audio file (wave)
	 */
	void registerSSResponse( void(*fp)(const std::string& data) );

	/*!
	 * @brief Setting signal processing interface (wave monitor) parameters.
	 * @param[in] language  language setting (ja, en, zh, ko, id, my, th, vi, fr, es)
	 * @param[in] engine  speech recognition engine setting (nict or google)
	 * (nict: supports ja, en, zh, ko, id, my, th, vi, fr, es; google: supports ja, en)
	 */
	void setSPIConfig( const std::string& language="ja", const std::string& engine="nict");

	/*!
	 * @brief play a sound file.
	 * @param[in] file_name sound file name (file location: rospeex/sound)
	 */
	void playSound( const std::string& file_name );

private:
	/*!
	 * @class impl class
	 */
	class Impl;
	boost::shared_ptr<Impl> impl_;
};

};



#endif // _ROSPEEX_INTERFACE_H_
